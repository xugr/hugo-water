# Hugo Water

A minimal reusable hugo theme based on [water.css](https://watercss.netlify.com/).

## Build

`npm run build:style`
